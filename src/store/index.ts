import { reactive, readonly } from "vue";
import orbitdb from 'orbit-db';
import {getIpfs, providers} from "ipfs-provider";
const { httpClient, jsIpfs, windowIpfs } = providers;

interface ProviderIPFS {
  ipfs: any;
  provider: string;
  apiAddress: undefined | string;
}

const ipfsProvider: ProviderIPFS = reactive({
  ipfs: {},
  provider: "",
  apiAddress: undefined
});

async function makeIPFSNode() {
  const newIpfsProvider = await getIpfs({
    // HTTP client library can be defined globally to keep code minimal
    // when httpClient provider is used multiple times
    loadHttpClientModule: () => require("ipfs-http-client"),
    // try window.ipfs (if present),
    // then http apis (if up),
    // and finally fallback to spawning embedded js-ipfs
    providers: [
      windowIpfs(),
      httpClient({
        apiAddress: "/ip4/127.0.0.1/tcp/5001"
      }),
      httpClient(),
      jsIpfs({
        // js-ipfs package is used only once, here
        loadJsIpfsModule: () => import(/* webpackChunkName: "ipfs" */ "ipfs"),
        options: {} // pass config: https://github.com/ipfs/js-ipfs/blob/master/packages/ipfs/docs/MODULE.md#ipfscreateoptions
      })
    ]
  });

  console.info(newIpfsProvider);

  Object.assign(ipfsProvider, newIpfsProvider);
}


makeIPFSNode();

export default {
  ipfsProvider: readonly(ipfsProvider)
};